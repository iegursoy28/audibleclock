package space.iegrsy.audibleclock;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {
    private static final long ONE_MINUTES = 60000;
    private static final long HALF_MINUTES = 30000;

    @BindView(R.id.radioGroup)
    public RadioGroup radioGroup;

    @BindView(R.id.btn_media_play)
    public ImageButton btnMediaPlay;

    @BindView(R.id.txtHumane)
    public TextView txtHumane;

    private long readPeriod = 2 * ONE_MINUTES;

    @OnClick({R.id.btn_read_start, R.id.btn_read_stop})
    public void onClickRead(View view) {
        switch (view.getId()) {
            case R.id.btn_read_start:
                StartService();
                break;
            case R.id.btn_read_stop:
                StopService();
                break;
        }
    }

    public static final String CMDTOGGLEPAUSE = "togglepause";
    public static final String CMDPAUSE = "pause";
    public static final String CMDPREVIOUS = "previous";
    public static final String CMDNEXT = "next";
    public static final String SERVICECMD = "com.android.music.musicservicecommand";
    public static final String CMDNAME = "command";
    public static final String CMDSTOP = "stop";

    @OnClick({R.id.btn_media_previous, R.id.btn_media_play, R.id.btn_media_next})
    public void onClickMedia(View view) {
        AudioManager mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        if (mAudioManager == null)
            return;

        switch (view.getId()) {
            case R.id.btn_media_previous: {
                if (mAudioManager.isMusicActive()) {
                    Intent i = new Intent(SERVICECMD);
                    i.putExtra(CMDNAME, CMDPREVIOUS);
                    sendBroadcast(i);
                }
                break;
            }
            case R.id.btn_media_play: {
                Intent i = new Intent(SERVICECMD);
                i.putExtra(CMDNAME, CMDTOGGLEPAUSE);
                sendBroadcast(i);
                if (mAudioManager.isMusicActive())
                    btnMediaPlay.setImageDrawable(getResources().getDrawable(android.R.drawable.ic_media_play));
                else
                    btnMediaPlay.setImageDrawable(getResources().getDrawable(android.R.drawable.ic_media_pause));
                break;
            }
            case R.id.btn_media_next: {
                if (mAudioManager.isMusicActive()) {
                    Intent i = new Intent(SERVICECMD);
                    i.putExtra(CMDNAME, CMDNEXT);
                    sendBroadcast(i);
                }
                break;
            }
        }
    }

    private boolean isRun = false;
    private int waitTime = 500;
    private Thread threadUpdater;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        ButterKnife.bind(this);
        radioGroup.setOnCheckedChangeListener(onCheckedChangeListener);
        radioGroup.check(R.id.radio_2m);
    }

    private RadioGroup.OnCheckedChangeListener onCheckedChangeListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            switch (checkedId) {
                case R.id.radio_30s: {
                    readPeriod = HALF_MINUTES;
                    break;
                }
                case R.id.radio_1m: {
                    readPeriod = ONE_MINUTES;
                    break;
                }
                case R.id.radio_1m30s: {
                    readPeriod = ONE_MINUTES + HALF_MINUTES;
                    break;
                }
                case R.id.radio_2m: {
                    readPeriod = ONE_MINUTES * 2;
                    break;
                }
                case R.id.radio_3m: {
                    readPeriod = ONE_MINUTES * 3;
                    break;
                }
                case R.id.radio_4m: {
                    readPeriod = ONE_MINUTES * 4;
                    break;
                }
                case R.id.radio_5m: {
                    readPeriod = ONE_MINUTES * 5;
                    break;
                }
                case R.id.radio_10m: {
                    readPeriod = ONE_MINUTES * 10;
                    break;
                }
                case R.id.radio_15m: {
                    readPeriod = ONE_MINUTES * 15;
                    break;
                }
            }
        }
    };

    private void StartService() {
        Intent intent = new Intent(this, ReadClockService.class);
        intent.putExtra(ReadClockService.EXTRA_READ_PERIOD, readPeriod);
        startService(intent);
    }

    private void StopService() {
        Intent intent = new Intent(this, ReadClockService.class);
        stopService(intent);
    }

    @Override
    protected void onDestroy() {
        StopService();
        super.onDestroy();
    }


    @Override
    protected void onResume() {
        super.onResume();

        threadUpdater = new Thread(runnable);
        threadUpdater.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        release();
    }

    private void release() {
        if (threadUpdater != null) {
            isRun = false;
            try {
                threadUpdater.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            threadUpdater = null;
        }
    }

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            isRun = true;

            while (isRun) {
                runOnUiThread(runnableUpdate);
                try {
                    Thread.sleep(waitTime);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    };

    private final Runnable runnableUpdate = new Runnable() {
        @Override
        public void run() {
            long ts = System.currentTimeMillis();
            String date = new SimpleDateFormat("dd/MM/yyyy\nHH:mm:ss", Locale.getDefault()).format(new java.util.Date(ts));
            txtHumane.setText(date);
        }
    };
}
