package space.iegrsy.audibleclock;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.speech.tts.TextToSpeech;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class ReadClockService extends Service {
    public static final String EXTRA_READ_PERIOD = "extra_read_period";

    private TextToSpeech toSpeech;
    private Thread runThread;

    private long periodSeconds = 120000;

    public ReadClockService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        Toast.makeText(this, "Service started. Time will be read at selected time intervals.", Toast.LENGTH_SHORT).show();

        toSpeech = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status != TextToSpeech.ERROR) {
                    toSpeech.setLanguage(Locale.getDefault());
                } else {
                    Toast.makeText(getBaseContext(), "Speech ERROR", Toast.LENGTH_LONG).show();
                }
            }
        });

        runThread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    try {
                        if (toSpeech != null) {
                            Date currentTime = Calendar.getInstance().getTime();
                            String text = String.format("%s %s %s", currentTime.getHours(), currentTime.getMinutes(), currentTime.getSeconds());
                            toSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, null);
                        }

                        Thread.sleep(periodSeconds);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                        break;
                    }
                }
            }
        });

        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null)
            periodSeconds = intent.getLongExtra(EXTRA_READ_PERIOD, 120000);

        if (runThread != null)
            runThread.start();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        if (runThread != null) {
            runThread.interrupt();
            runThread = null;
        }

        Toast.makeText(this, "Service stopped.", Toast.LENGTH_SHORT).show();
        super.onDestroy();
    }
}
